/**
 * Created by jules on 18/07/17.
 */

import Vue from 'vue';
import Dictionary from '@/components/DictionaryHome';

describe('Dictionary.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Dictionary);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('h1').textContent)
      .to.equal('Dictionary Explorer');
  });
});
