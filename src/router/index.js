import Vue from 'vue';
import Router from 'vue-router';
import DictionaryHome from '@/components/DictionaryHome';
import DifferenceHome from '@/components/DifferenceHome';
import MainHome from '@/components/MainHome';
import Source from '@/components/SourceJson';
import Application from '@/components/Application';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/app',
      name: 'application',
      component: Application,
      redirect: '/app/home',
      children: [
        {
          path: 'home',
          name: 'home',
          component: MainHome,
        },
        {
          path: 'dictionary',
          name: 'dictionary',
          component: DictionaryHome,
        },
        {
          path: 'difference',
          name: 'difference',
          component: DifferenceHome,
        },
      ],
    },
    {
      path: '/source',
      name: 'source',
      component: Source,
    },
    {
      path: '*',
      redirect: '/app/home',
    },
  ],
});
