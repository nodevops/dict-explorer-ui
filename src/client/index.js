import axios from 'axios';
import { getConfig } from '../configuration';

export function getActuator() {
  const configuration = getConfig();
  return {
    url: `${configuration.BACKEND_URL}${configuration.ACTUATOR_HEALTH_ENDPOINT}`,
  };
}

export function fetchActuator() {
  return axios
    .get(getActuator().url)
    .then(response => response.data);
}

export function getDictionary() {
  const configuration = getConfig();
  return {
    url: `${configuration.BACKEND_URL}${configuration.API_VERSION}/dict`,
  };
}

export function fetchDictionary(dict) {
  return axios
    .post(getDictionary().url, dict)
    .then(response => response.data);
}

export function getDifference() {
  const configuration = getConfig();
  return {
    url: `${configuration.BACKEND_URL}${configuration.API_VERSION}/diff`,
  };
}

export function fetchDifference(biDict) {
  return axios
    .post(getDifference().url, biDict)
    .then(response => response.data);
}
