import axios from 'axios';

let config = {};

export function getConfig() {
  return config;
}

export function fetchConfig() {
  return axios
    .get('static/config.json')
    .then((response) => {
      config = response.data;
    });
}
