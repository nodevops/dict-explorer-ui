FROM nginx:1.12.1-alpine

######################################################################
# confd installation
######################################################################

RUN mkdir -p /usr/local/appli/config && \
    apk add --no-cache wget && \
    wget --no-check-certificate -O /usr/local/bin/confd https://github.com/kelseyhightower/confd/releases/download/v0.11.0/confd-0.11.0-linux-amd64 && \
    chmod +x /usr/local/bin/confd

######################################################################
# environment vaiables
######################################################################

ENV EXPLORER_BACKEND_URL "http://localhost:8080"
ENV EXPLORER_ACTUATOR_HEALTH_ENDPOINT "/admin/health"
ENV EXPLORER_API_VERSION "/api/v1"
# ARG GIT_SHA1
# ARG GIT_BRANCH
# ARG CI_BUILD_NUMBER
# ENV GIT_SHA1=${GIT_SHA1} GIT_BRANCH=${GIT_BRANCH} CI_BUILD_NUMBER=${CI_BUILD_NUMBER}

######################################################################
# import configuration
######################################################################

COPY runtime/confd /etc/confd/
COPY runtime/startup.sh /usr/local/bin/
COPY runtime/nginx-default.conf /etc/nginx/conf.d/default.conf
RUN  chmod +x /usr/local/bin/startup.sh

######################################################################
# import web app
######################################################################

COPY dist /usr/share/nginx/html

######################################################################
# launch confd and nginx
######################################################################

CMD "/usr/local/bin/startup.sh"
